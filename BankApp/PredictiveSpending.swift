//
//  PredictiveSpending.swift
//  BankApp
//
//  Created by Yang Chen on 2014-09-17.
//  Copyright (c) 2014 Yang Chen. All rights reserved.
//

import Foundation

// Playground - noun: a place where people can play

import UIKit

/*extension NSDate
    {
    convenience
    //init(dateString:String) {
        let dateStringFormatter = NSDateFormatter()
        dateStringFormatter.dateFormat = "yyyy-MM-dd"
        dateStringFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        let d = dateStringFormatter.dateFromString(dateString)
        self.init(timeInterval:0, sinceDate:d!)
    }
}*/

/*
var date9 = NSDate(dateString:"2014-06-06")
var date8 = NSDate(dateString:"2014-06-05")
var date7 = NSDate(dateString:"2014-05-31")
var date6 = NSDate(dateString:"2014-05-29")
var date5 = NSDate(dateString:"2014-05-20")
var date4 = NSDate(dateString:"2014-05-19")
var date3 = NSDate(dateString:"2014-05-08")
var date2 = NSDate(dateString:"2014-05-05")
var date1 = NSDate(dateString:"2014-04-30")
var date0 = NSDate(dateString:"2014-04-28")

let trans:[Double] = [-45.32,10, -23.1, -23.3,55.2, -22.1, -33, 80.41, -22.35]
let vendors = ["Walmart", "University of Waterloo", "Walmart", "Walmart", "ABC", "X", "ABC", "Y", "ABC", "Walmart"]
let dates:[NSDate] = [date0, date1, date2, date3, date4, date5, date6, date7, date8, date9]

var numCount = 0
var indicies:[Int] = []
var r_vendors:[String] = []
var timeDiffs:[Int] = []
var multiple = false

func search(check:Int, indexes: [Int]) -> Bool{
    for searched in indicies
    {
        if (check == searched)
        {
            return false
        }
    }
    return true
}

func findRepeat()
{
    for i in 0..<vendors.count {
        if (search(i, indicies))
        {
            let temp = vendors[i]
            for j in (i+1)..<vendors.count{
                let temp2 = vendors[j]
                if (temp == temp2)
                {
                    if (multiple == false)
                    {
                        indicies += [i]
                        r_vendors += [temp]
                    }
                    multiple = true
                    indicies += [j]
                    r_vendors += [temp2]
                }
            }
            multiple = false
        }
    }
    
    for i in 0..<(r_vendors.count-1) {
        if (r_vendors[(i+1)] == r_vendors[i])
        {
            var diff = dates[indicies[(i+1)]].timeIntervalSinceDate(dates[indicies[i]])
            diff = diff/86400
            println("\(diff) days between \(r_vendors[i]) on \(dates[indicies[i]]) and \(r_vendors[i+1]) on \(dates[indicies[(i+1)]])")
        }
    }

}
//var array = Array(count:7, repeatedValue:Array(count:2, repeatedValue:Int()))*/
