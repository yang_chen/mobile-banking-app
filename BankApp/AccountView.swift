//
//  AccountView.swift
//  BankApp
//
//  Created by Yang Chen on 2014-09-10.
//  Copyright (c) 2014 Yang Chen. All rights reserved.
//

import UIKit

class AccountView: UIViewController , UITableViewDelegate{
    
    var accountBalances = ["Chequing (#562832)": "$3042", "Savings (#2234078)": "$13,000"]
    
    var typeList:[String] {
        get{
            return Array(accountBalances.keys)
        }
    }
    
    var amountList:[String] {
        get{
            return Array(accountBalances.values)
        }
    }
    
    @IBOutlet var tableView: UITableView!
    
    //var items: [String] = ["we", "Heart", "Swift"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        return self.typeList.count;
    }
    
    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        
        //var cell:UITableViewCell = self.tableView.dequeueReusableCellWithIdentifier("cell") as UITableViewCell
        var cell: UITableViewCell = self.tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as UITableViewCell
        
        
        cell.textLabel?.text = self.typeList[indexPath.row]
        cell.detailTextLabel?.text = self.amountList[indexPath.row]
        
        return cell
    }
    
    func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!){
        println("You selected cell #\(indexPath.row)!")
    }
    
    
}
