//
//  GraphView.swift
//  BankApp
//
//  Created by Yang Chen on 2014-09-11.
//  Copyright (c) 2014 Yang Chen. All rights reserved.
//

import UIKit

extension NSDate
    {
    convenience
    init(dateString2:String) {
        let dateStringFormatter = NSDateFormatter()
        dateStringFormatter.dateFormat = "yyyy-MM-dd"
        dateStringFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        let d = dateStringFormatter.dateFromString(dateString2)
        self.init(timeInterval:0, sinceDate:d!)
    }
}

class PredictionGraph: UIViewController{
    
    @IBOutlet var myPGraph : BEMSimpleLineGraphView!
    
    let backgroundColor = UIColor.init(red: 0.0431372549, green: 0.34509803921, blue: 0.18039215686, alpha: 1.0)
    let axisColor = UIColor.whiteColor()
    
    let trans:[Double] = [-45.32,10, -23.1, -23.3,55.2, -22.1, -33, 80.41, -22.35]
    
    var date9 = NSDate(dateString:"2014-09-06")
    var date8 = NSDate(dateString:"2014-08-05")
    var date7 = NSDate(dateString:"2014-07-31")
    var date6 = NSDate(dateString:"2014-06-29")
    var date5 = NSDate(dateString:"2014-05-20")
    var date4 = NSDate(dateString:"2014-04-19")
    var date3 = NSDate(dateString:"2014-03-08")
    var date2 = NSDate(dateString:"2014-02-05")
    var date1 = NSDate(dateString:"2014-01-30")
    let date0 = NSDate(dateString:"2014-04-28")
    
    let vendors = ["Walmart", "University of Waterloo", "Walmart", "Walmart", "ABC", "X", "ABC", "Y", "ABC", "Walmart"]
    //let dates:[NSDate] = [date0, date1, date2, date3, date4, date5, date6, date7, date8, date9]
    let dates:[NSDate] = [NSDate(dateString:"2014-01-05"), NSDate(dateString:"2014-02-06"), NSDate(dateString:"2014-03-07"), NSDate(dateString:"2014-04-08"), NSDate(dateString:"2014-05-09"), NSDate(dateString:"2014-06-07"), NSDate(dateString:"2014-07-07"), NSDate(dateString:"2014-08-07"), NSDate(dateString:"2014-09-07")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "rotated2", name: UIDeviceOrientationDidChangeNotification, object: nil)
        
        var myData = [
            ["label" : "2014-06-06",   "value" : NSNumber(int:15)],
            ["label" : "2014-06-07",  "value" : NSNumber(int:30)],
            ["label" : "2014-06-08",  "value" : NSNumber(int:7)],
            ["label" : "2014-06-09", "value" : NSNumber(int:60)],
            //["label" : "May",   "value" : NSNumber(int:30)],
            //["label" : "April",   "value" : NSNumber(int:15)],
            //["label" : "March",   "value" : NSNumber(int:45)],
            ] as NSArray
        
        //let graph = Graph_View(frame: CGRectMake(50, 130, 900, 600), data: myData)
        //self.view.addSubview(graph)
        self.myPGraph.colorTop = backgroundColor
        self.myPGraph.colorBottom = backgroundColor
        self.myPGraph.colorYaxisLabel = axisColor
        self.myPGraph.colorXaxisLabel = axisColor
        self.myPGraph.sizePoint = 10.0
        var myPGraph = BEMSimpleLineGraphView(frame:CGRectMake(0, 0, 320, 200));
        self.view.addSubview(myPGraph)
        
        self.myPGraph.enableYAxisLabel = true
        //self.myGraph.autoScaleYAxis = true
        self.myPGraph.alwaysDisplayDots = true
        self.myPGraph.enableReferenceAxisLines = true
        self.myPGraph.enableReferenceAxisFrame = true
        self.myPGraph.enablePopUpReport = true
        self.myPGraph.enableTouchReport = true
        
        //self.labelValues.text = [NSString stringWithFormat:@"%i", [[self.myGraph calculatePointValueSum] intValue]];
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func rotated2()
    {
        if(UIDeviceOrientationIsPortrait(UIDevice.currentDevice().orientation))
        {
            self.performSegueWithIdentifier("GraphToList", sender: self)
        }
    }
    
    func numberOfPointsInLineGraph(graph: BEMSimpleLineGraphView) -> NSInteger
    {
        return trans.count
    }
    
    func lineGraph(graph: BEMSimpleLineGraphView, valueForPointAtIndex: NSInteger) -> CGFloat
    {
        println(CGFloat(trans[valueForPointAtIndex]))
        return (CGFloat(trans[valueForPointAtIndex]))
    }
    
    func lineGraph(graph: BEMSimpleLineGraphView, labelOnXAxisForIndex: NSInteger) -> NSString
    {
        var dateFormatter: NSDateFormatter = NSDateFormatter()
        var dateStyle: NSDateFormatterStyle
        
        dateFormatter.dateFormat = "MM\nyyyy"
        //.setDateStyle(NSDateFormatterStyle.MediumStyle())
        
        return dateFormatter.stringFromDate(dates[labelOnXAxisForIndex])
    }
    
    func numberOfYAxisLabelsOnLineGraph(graph: BEMSimpleLineGraphView) -> NSInteger
    {
        return 5;
    }
    
    
}