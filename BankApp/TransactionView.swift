//
//  TransactionView.swift
//  BankApp
//
//  Created by Yang Chen on 2014-09-11.
//  Copyright (c) 2014 Yang Chen. All rights reserved.
//

import UIKit

class TransactionView: UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "rotated1", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func rotated1()
    {
        
        if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation))
        {
            self.performSegueWithIdentifier("ListToGraph", sender: self)
        }
        
    }
    
}