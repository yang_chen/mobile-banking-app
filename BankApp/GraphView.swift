//
//  GraphView.swift
//  BankApp
//
//  Created by Yang Chen on 2014-09-11.
//  Copyright (c) 2014 Yang Chen. All rights reserved.
//

import UIKit

extension NSDate
    {
    convenience
    init(dateString:String) {
        let dateStringFormatter = NSDateFormatter()
        dateStringFormatter.dateFormat = "yyyy-MM-dd"
        dateStringFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        let d = dateStringFormatter.dateFromString(dateString)
        self.init(timeInterval:0, sinceDate:d!)
    }
}

class GraphView: UIViewController{
    
    @IBOutlet var myGraph : BEMSimpleLineGraphView!
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    
    let backgroundColor = UIColor.init(red: 0.0431372549, green: 0.34509803921, blue: 0.18039215686, alpha: 1.0)
    let axisColor = UIColor.whiteColor()
    
    var trans:[Double] = [-45.32,10, -23.1, -23.3,55.2, -22.1, -33, 80.41, -22.35]
    
    var average: Double = 0
    var a_num: Double = 0
    
    var date9 = NSDate(dateString:"2014-09-06")
    var date8 = NSDate(dateString:"2014-08-05")
    var date7 = NSDate(dateString:"2014-07-31")
    var date6 = NSDate(dateString:"2014-06-29")
    var date5 = NSDate(dateString:"2014-05-20")
    var date4 = NSDate(dateString:"2014-04-19")
    var date3 = NSDate(dateString:"2014-03-08")
    var date2 = NSDate(dateString:"2014-02-05")
    var date1 = NSDate(dateString:"2014-01-30")
    let date0 = NSDate(dateString:"2014-04-28")
    
    let vendors = ["Walmart", "University of Waterloo", "Walmart", "Walmart", "ABC", "X", "ABC", "Y", "ABC", "Walmart"]
    //let dates:[NSDate] = [date0, date1, date2, date3, date4, date5, date6, date7, date8, date9]
    var dates:[NSDate] = [NSDate(dateString:"2013-12-05"), NSDate(dateString:"2014-01-05"), NSDate(dateString:"2014-02-06"), NSDate(dateString:"2014-03-07"), NSDate(dateString:"2014-04-08"), NSDate(dateString:"2014-05-09"), NSDate(dateString:"2014-06-07"), NSDate(dateString:"2014-07-07"), NSDate(dateString:"2014-08-07"), NSDate(dateString:"2014-09-07")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "rotated2", name: UIDeviceOrientationDidChangeNotification, object: nil)
        
        button1.hidden=true
        
        var myData = [
            ["label" : "2014-06-06",   "value" : NSNumber(int:15)],
            ["label" : "2014-06-07",  "value" : NSNumber(int:30)],
            ["label" : "2014-06-08",  "value" : NSNumber(int:7)],
            ["label" : "2014-06-09", "value" : NSNumber(int:60)],
            //["label" : "May",   "value" : NSNumber(int:30)],
            //["label" : "April",   "value" : NSNumber(int:15)],
            //["label" : "March",   "value" : NSNumber(int:45)],
            ] as NSArray
        
        //let graph = Graph_View(frame: CGRectMake(50, 130, 900, 600), data: myData)
        //self.view.addSubview(graph)
        self.myGraph.colorTop = backgroundColor
        self.myGraph.colorBottom = backgroundColor
        self.myGraph.colorYaxisLabel = axisColor
        self.myGraph.colorXaxisLabel = axisColor
        self.myGraph.sizePoint = 10.0
        var myGraph = BEMSimpleLineGraphView(frame:CGRectMake(0, 0, 320, 200));
        self.view.addSubview(myGraph)
        
        self.myGraph.enableYAxisLabel = true
        //self.myGraph.autoScaleYAxis = true
        self.myGraph.alwaysDisplayDots = true
        self.myGraph.enableReferenceAxisLines = true
        self.myGraph.enableReferenceAxisFrame = true
        self.myGraph.enablePopUpReport = true
        self.myGraph.enableTouchReport = true
        
        //self.labelValues.text = [NSString stringWithFormat:@"%i", [[self.myGraph calculatePointValueSum] intValue]];
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func rotated2()
    {
        if(UIDeviceOrientationIsPortrait(UIDevice.currentDevice().orientation))
        {
            self.performSegueWithIdentifier("GraphToList", sender: self)
        }
    }
    
    func numberOfPointsInLineGraph(myGraph: BEMSimpleLineGraphView) -> NSInteger
    {
        return trans.count
    }
    
    func lineGraph(myGraph: BEMSimpleLineGraphView, valueForPointAtIndex: NSInteger) -> CGFloat
    {
        println(CGFloat(trans[valueForPointAtIndex]))
        return CGFloat(3042+trans[valueForPointAtIndex])
    }
    
    func lineGraph(myGraph: BEMSimpleLineGraphView, labelOnXAxisForIndex: NSInteger) -> NSString
    {
        var dateFormatter: NSDateFormatter = NSDateFormatter()
        var dateStyle: NSDateFormatterStyle
        
        dateFormatter.dateFormat = "MM\nyyyy"
        //.setDateStyle(NSDateFormatterStyle.MediumStyle())
        
        return dateFormatter.stringFromDate(dates[labelOnXAxisForIndex])
    }
    
    func numberOfYAxisLabelsOnLineGraph(myGraph: BEMSimpleLineGraphView) -> NSInteger
    {
        return 5;
    }
    
    func lineGraph(myGraph: BEMSimpleLineGraphView, didTouchGraphWithCloestIndex: NSInteger)
    {
        //self.myGraph.labelValues.text = [NSString stringWithFormat:@"%@", [self.ArrayOfValues objectAtIndex:index]];
        //self.myGraph.labelDates.text = [NSString stringWithFormat:@"in %@", [self.ArrayOfDates objectAtIndex:index]];
    }
    
    func lineGraph(myGraph: BEMSimpleLineGraphView, didReleaseTouchFromGraphWithClosestIndex: CGFloat)
    {
    }
    
    @IBAction func buttonTapped(sender: UIButton) {
        button2.hidden=true
        button1.hidden=false
        
        var numCount = 0
        var indicies:[Int] = []
        var r_vendors:[String] = []
        var timeDiffs:[Int] = []
        var multiple = false
        
        
        for i in 0..<vendors.count {
            if (search2(i, indicies: indicies))
            {
                let temp = vendors[i]
                for j in (i+1)..<vendors.count{
                    let temp2 = vendors[j]
                    if (temp == temp2)
                    {
                        if (multiple == false)
                        {
                            indicies += [i]
                            println("i: \(i)")
                            r_vendors += [temp]
                        }
                        multiple = true
                        indicies += [j]
                        println("j: \(j)")
                        r_vendors += [temp2]
                    }
                }
                multiple = false
            }
        }
        
        for i in 0..<(r_vendors.count-1) {
            if (r_vendors[(i+1)] == r_vendors[i])
            {
                var diff = dates[indicies[(i+1)]].timeIntervalSinceDate(dates[indicies[i]])
                diff = diff/86400
                println("\(diff) days between \(r_vendors[i]) on \(dates[indicies[i]]) and \(r_vendors[i+1]) on \(dates[indicies[(i+1)]])")
                
                if (diff>28 && diff < 35)
                {
                    average += trans[indicies[i]]
                    println("average1: \(average)")
                    a_num += 1
                }
            }
        }
        average = average/a_num
        println("average: \(average)")
        trans = [-45.32,10, -23.1, -23.3,55.2, -22.1, -33, 80.41, -22.35, average]
        dates = [NSDate(dateString:"2013-12-05"), NSDate(dateString:"2014-01-05"), NSDate(dateString:"2014-02-06"), NSDate(dateString:"2014-03-07"), NSDate(dateString:"2014-04-08"), NSDate(dateString:"2014-05-09"), NSDate(dateString:"2014-06-07"), NSDate(dateString:"2014-07-07"), NSDate(dateString:"2014-08-07"), NSDate(dateString:"2014-09-07"), NSDate(dateString:"2014-10-07")]
        self.myGraph.reloadGraph()
    }
    
    
    func search2(check:Int, indicies: [Int]) -> Bool{
        for searched in indicies
        {
            if (check == searched)
            {
                return false
            }
        }
        return true
    }

    @IBAction func disableP(sender: UIButton) {
        button2.hidden=false
        button1.hidden=true
        
        trans = [-45.32,10, -23.1, -23.3,55.2, -22.1, -33, 80.41, -22.35]
        let vendors = ["Walmart", "University of Waterloo", "Walmart", "Walmart", "ABC", "X", "ABC", "Y", "ABC", "Walmart"]
        //let dates:[NSDate] = [date0, date1, date2, date3, date4, date5, date6, date7, date8, date9]
        var dates:[NSDate] = [NSDate(dateString:"2013-12-05"), NSDate(dateString:"2014-01-05"), NSDate(dateString:"2014-02-06"), NSDate(dateString:"2014-03-07"), NSDate(dateString:"2014-04-08"), NSDate(dateString:"2014-05-09"), NSDate(dateString:"2014-06-07"), NSDate(dateString:"2014-07-07"), NSDate(dateString:"2014-08-07"), NSDate(dateString:"2014-09-07")]
        average = 0
        self.myGraph.reloadGraph()
    }
}